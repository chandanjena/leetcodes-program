package com.chandan.treegraph;

public class MyTreeImplement implements MyTree{

    @Override
    public TreeNode createNode() {
        TreeNode root = new TreeNode(5);
        root.left = new TreeNode(7);
        root.right = new TreeNode(10);
        root.left.left = new TreeNode(11);
        root.left.right = new TreeNode(19);
        root.right.left = new TreeNode(25);
        root.right.right = new TreeNode(30);
        return root;
    }

    @Override
    public void inorderTraversal(TreeNode root) {
        if(root == null) return;
        inorderTraversal(root.left);
        System.out.print(root.data + " ");
        inorderTraversal(root.right);
    }

    @Override
    public void preOrderTraversal(TreeNode root) {
        if(root == null) return;
        System.out.print(root.data+" ");
        preOrderTraversal(root.left);
        preOrderTraversal(root.right);
    }

    @Override
    public void postOrderTraversal(TreeNode root) {
        if (root == null) return;
        postOrderTraversal(root.left);
        postOrderTraversal(root.right);
        System.out.print(root.data+ " ");
    }

    @Override
    public int sumOfAllNodes(TreeNode root) {
        if (root == null) return 0;
        return root.data + sumOfAllNodes(root.left) + sumOfAllNodes(root.right);
    }

    @Override
    public int getOddEvenDifferenceUsingRecursion(TreeNode root) {
        if(root == null) return 0;
        return root.data - getOddEvenDifferenceUsingRecursion(root.left) -
                getOddEvenDifferenceUsingRecursion(root.right);
    }

    @Override
    public int getNumberOfNode(TreeNode root) {
        if (root == null) return 0;
        return 1+ getNumberOfNode(root.left) + getNumberOfNode(root.right);
    }

    @Override
    public int getTotalNumberOfLeafNode(TreeNode root) {
        if(root == null) return 0;
        if(root.left == null && root.right == null) return 1;
        return getTotalNumberOfLeafNode(root.left) + getTotalNumberOfLeafNode(root.right);
    }

    @Override
    public int getHeightOfNode(TreeNode root) {
        if(root == null) return -1;
        return max(getHeightOfNode(root.left) , getHeightOfNode(root.right)) + 1;
    }

    private int max(int i, int j) {
        return Math.max(i, j);
    }

}
