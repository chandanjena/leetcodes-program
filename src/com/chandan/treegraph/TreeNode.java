package com.chandan.treegraph;

public class TreeNode {
    int data;
    TreeNode left;
    TreeNode right;
    TreeNode(){
        this.data = 0;
        this.left = null;
        this.right = null;
    }
    TreeNode(int data){
        this.data = data;
        this.left = null;
        this.right = null;
    }

    @Override
    public String toString() {
        return "TreeNode{" +
                "data=" + data +
                ", left=" + left +
                ", right=" + right +
                '}';
    }
}
