package com.chandan.treegraph;

public interface MyTree {
    TreeNode createNode();
    void inorderTraversal(TreeNode root);
    void preOrderTraversal(TreeNode root);
    void postOrderTraversal(TreeNode root);
    int sumOfAllNodes(TreeNode root);
    int getOddEvenDifferenceUsingRecursion(TreeNode root);
    int getNumberOfNode(TreeNode root);
    int getTotalNumberOfLeafNode(TreeNode root);
    int getHeightOfNode(TreeNode root);
}
