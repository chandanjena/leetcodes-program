package com.chandan.treegraph;

public class TreeClient {
    public static void main(String[] args) {
        MyTreeImplement myTree = new MyTreeImplement();
        TreeNode root = myTree.createNode();
        System.out.println("preorder");
        myTree.preOrderTraversal(root);
        System.out.println("\ninorder");
        myTree.inorderTraversal(root);
        System.out.println("\npost");
        myTree.postOrderTraversal(root);
        System.out.println("sum of all nodes");
        System.out.println(myTree.sumOfAllNodes(root));
        System.out.println("level oder value:-"+ myTree.getOddEvenDifferenceUsingRecursion(root));
        System.out.println("total number of nodes present:-" + myTree.getNumberOfNode(root));
        System.out.println("total leaf nodes:-"+ myTree.getTotalNumberOfLeafNode(root));
        System.out.println("height of the node:-"+myTree.getHeightOfNode(root.left.right));
    }
}
