package com.chandan.heapmatrix;

import java.util.Collections;
import java.util.PriorityQueue;

/**
 * find the kth largest element a list
 */
public class FindKthLargestElement {
    public static void main(String[] args) {
        int []arr = {10,7,11,5,2,13,1,45};
        int k = 3;
        System.out.println(findKLargest(arr,k));
    }

    private static int findKLargest(int[] arr, int k) {
        PriorityQueue<Integer> heap = new PriorityQueue<>(Collections.reverseOrder());
        for (int j : arr) {
            heap.add(j);
        }
        for(int i =0; i<k-1;i++){
            heap.poll();
        }
        return heap.peek();
    }

}
