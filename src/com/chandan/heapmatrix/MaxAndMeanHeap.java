package com.chandan.heapmatrix;

import java.util.Collections;
import java.util.PriorityQueue;

public class MaxAndMeanHeap {
    public static void main(String[] args) {
        int []arr = {10,7,11,5,2,13,1,45};
        MaxAndMeanHeap.maxHeap(arr);
    }
    public static void minHeap(int []arr){
        PriorityQueue<Integer> heap =   new PriorityQueue<>();
        for (int j : arr) {
            heap.add(j);
        }
        for (int i = 0; i < arr.length; i++){
            System.out.println(heap.peek());
            heap.poll();
        }
    }
    public static void maxHeap(int []arr) {
        PriorityQueue<Integer> heap = new PriorityQueue<>(Collections.reverseOrder());
        for (int j : arr) {
            heap.add(j);
        }
        for (int i = 0; i < arr.length; i++){
            System.out.println(heap.peek());
            heap.poll();
        }
    }
}
