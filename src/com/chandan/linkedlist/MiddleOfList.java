package com.chandan.linkedlist;

public class MiddleOfList {
    public static void main(String[] args) {
        ListCreator listCreator = new ListFactory();
        Node node = listCreator.createList();
        printList(node);
        Node middle = getMiddle(node);
        System.out.println("the middle node data is:-" + middle.data);
    }
    public static void printList(Node node) {
        while(node != null){
            System.out.print(node.data +",");
            node = node.next;
        }
    }
    public static Node getMiddle(Node node){
        if( node == null) {
            return null;
        }
        Node slow = node;
        Node fast = node.next;
        while (fast != null && fast.next != null){
            slow = slow.next;
            fast = fast.next.next;
        }
        return slow;
    }
}
