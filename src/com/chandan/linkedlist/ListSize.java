package com.chandan.linkedlist;

public class ListSize {
    public static void main(String[] args) {
        ListCreator listCreator = new ListFactory();
        System.out.println("size of the list:-"+ sizeOfList(listCreator));
    }

    private static int sizeOfList(ListCreator listCreator) {
        Node head = listCreator.createList();
        int count = 0;
        while (head != null){
            count++;
            head = head.next;
        }
        return count;
    }
}
