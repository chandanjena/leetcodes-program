package com.chandan.linkedlist;
public class ReverseList {
    public static void main(String[] args) {
        ListCreator listCreator = new ListFactory();
        Node node = listCreator.createList();
        printList(node);
        Node reverseNode = reverse(node);
        System.out.println();
        printList(reverseNode);

    }
    public static void printList(Node node) {
        while(node != null){
            System.out.print(node.data +",");
            node = node.next;
        }
    }
    private static Node reverse(Node node) {
        if( node == null || node.next == null) {
            return node;
        }
        Node temp = reverse(node.next);
        node.next.next = node;
        node.next = null;
        return temp;
    }
}
