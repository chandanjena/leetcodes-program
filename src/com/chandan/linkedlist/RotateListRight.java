package com.chandan.linkedlist;

/**
 * Example
 * list 8 9 10 11 12 13 14
 * rotate by 1
 * 14 8 9 10 11 12 13
 *
 * solution
 * 1. make a list to circular
 * 2. the go till k-1 position and make the node as null
 * 3  traverse fro k node till null and point it to head
 */
public class RotateListRight {
    public static void main(String[] args) {
        ListCreator listCreator = new ListFactory();
        Node head = listCreator.createList();
        Node afterRotateHead = rotate(head, 1);
        while (afterRotateHead != null){
            System.out.println(afterRotateHead.data);
            afterRotateHead = afterRotateHead.next;
        }
    }

    private static Node rotate(Node head, int k) {
        if(k == 0){
            return head;
        }
        Node current = head;
        // making circular
        while (current.next != null)
            current = current.next;
        current.next = head;
        current = head;
        // traverse the linked list to k-1 position which
        // will be last element for rotated array.
        for (int i = 0; i < k - 1; i++)
            current = current.next;
        head = current.next;
        current.next = null;
        return head;
    }
}
