package com.chandan.linkedlist;

public class SortUnOrderList {
    public static void main(String[] args) {
        ListCreator listCreator = new ListFactory();
        Node node = listCreator.createUnOrderList();
        // before sort
        print(node);
        Node sortedList = mergeSort(node);
        System.out.println();
        print(sortedList);
    }
    public static void print(Node node) {
        if (node == null){
            return;
        }
        System.out.println(node.data+" ");
        print(node.next);
    }
    public static Node mergeSort(Node node) {
        if(node == null || node.next == null) {
            return node;
        }
        Node middle = middleNode(node);
        Node secondHalf = middle.next;
        middle.next = null;
        return merge(mergeSort(node) , mergeSort(secondHalf));
    }
    public static Node middleNode(Node node) {
        if(node == null){
            return null;
        }
        Node slow = node;
        Node fast = node.next;
        while (fast != null && fast.next != null) {
            slow = slow.next;
            fast = fast.next.next;
        }
        return slow;
    }
    public static Node merge(Node a, Node b) {
        Node temp = new Node(0);
        Node finalList = temp;
        while (a != null && b != null){
            if(a.data < b.data){
                temp.next = a;
                a = a.next;
            } else {
                temp.next = b;
                b = b.next;
            }
            temp = temp.next;
        }
        temp.next = (a==null) ? b : a ;
        return finalList.next;
    }
}
