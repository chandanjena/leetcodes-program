package com.chandan.linkedlist;

public class NodeSearch {
    public static void main(String[] args) {
        ListCreator listCreator = new ListFactory();
        int element = 60;
        if(search(listCreator, element)) {
            System.out.println("present");
        } else{
            System.out.println("not present");
        }
    }

    private static boolean search(ListCreator listCreator, int element) {
        Node temp = listCreator.createList();
        if (temp == null){
            return false;
        }
        while (temp != null){
            if(temp.data == element){
                return true;
            }
            temp = temp.next;
        }
        return false;
    }
}
