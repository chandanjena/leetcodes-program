package com.chandan.linkedlist;

public interface ListCreator {
    Node createList();
}
