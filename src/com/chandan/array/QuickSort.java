package com.chandan.array;

/**
 * Set the first index of the array to left and loc variable. Set the last index of the array to right variable. i.e. left = 0, loc = 0, en d = n - 1, where n is the length of the array.
 * Start from the right of the array and scan the complete array from right to beginning comparing each element of the array with the element pointed by loc.
 * Ensure that, a[loc] is less than a[right].
 *
 * If this is the case, then continue with the comparison until right becomes equal to the loc.
 * If a[loc] > a[right], then swap the two values. And go to step 3.
 * Set, loc = right
 * start from element pointed by left and compare each element in its way with the element pointed by the variable loc. Ensure that a[loc] > a[left]
 * if this is the case, then continue with the comparison until loc becomes equal to left.
 * [loc] < a[right], then swap the two values and go to step 2.
 * Set, loc = left.
 *
 * Complexity
 * time - O(nlogn)
 * space - O(logn)
 */
public class QuickSort {
    public static void main(String[] args) {
        int i;
        int[] arr={90,23,101,45,65,23,67,89,34,23};
        quickSort(arr, 0, 9);
        System.out.println("\n The sorted array is: \n");
        for(i=0;i<10;i++)
            System.out.println(arr[i]);
    }
    static void quickSort(int[] a, int beg, int end)
    {

        int loc;
        if(beg<end)
        {
            loc = partition(a, beg, end);
            quickSort(a, beg, loc-1);
            quickSort(a, loc+1, end);
        }
    }
    public static int partition(int[] a, int beg, int end)
    {

        int left, right, temp, loc, flag;
        loc = left = beg;
        right = end;
        flag = 0;
        while(flag != 1)
        {
            while((a[loc] <= a[right]) && (loc!=right))
                right--;
            if(loc==right) {
                flag = 1;
            }
            else if(a[loc]>a[right])
            {
                temp = a[loc];
                a[loc] = a[right];
                a[right] = temp;
                loc = right;
            }
            if(flag!=1)
            {
                while((a[loc] >= a[left]) && (loc!=left))
                    left++;
                if(loc==left)
                    flag =1;
                else if(a[loc] <a[left])
                {
                    temp = a[loc];
                    a[loc] = a[left];
                    a[left] = temp;
                    loc = left;
                }
            }
        }
        return loc;
    }
}
