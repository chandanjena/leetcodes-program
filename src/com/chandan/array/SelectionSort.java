package com.chandan.array;

import java.util.Arrays;

/**
 * algorithm
 * Step 1 − Set MIN to location 0
 * Step 2 − Search the minimum element in the list
 * Step 3 − Swap with value at location MIN
 * Step 4 − Increment MIN to point to next element
 * Step 5 − Repeat until list is sorted
 *
 * comlexity
 * O(n2)-time complexity
 * O(1)-space complexity
 */
public class SelectionSort {
    public static void main(String[] args) {
        int [] arr = {14,33,27,10,35,19,42,44};
        selectionSort(arr);
        Arrays.stream(arr).forEach(System.out::println);
    }

    private static void selectionSort(int[] arr) {
        if(arr.length <= 1){
            return;
        }
        for(int i =0; i<arr.length; i++){
            int minIndex = i;
            for(int j = i +1; j < arr.length; j++){
                if(arr[minIndex] > arr[j]){
                    minIndex = j;
                }
            }
            if(minIndex != i){
                int tmp = arr[minIndex];
                arr[minIndex] = arr[i];
                arr[i] = tmp;
            }
        }
    }
}
