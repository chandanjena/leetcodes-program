package com.chandan.array;

import java.util.Arrays;

/**
 * Step 1 − If it is the first element, it is already sorted. return 1;
 * Step 2 − Pick next element
 * Step 3 − Compare with all elements in the sorted sub-list
 * Step 4 − Shift all the elements in the sorted sub-list that is greater than the
 *          value to be sorted
 * Step 5 − Insert the value
 * Step 6 − Repeat until list is sorted
 *
 * Complexity
 * TC-Ο(n2) and not suitable for large data set
 * SC= O(1) because no extra space is necessary
 */
public class InsertSort {
    public static void main(String[] args) {
        int []arr = {5,2,14,6,3};
        int key;
        for(int i = 1  ; i < arr.length; i++){
            key = arr[i];
            int j ;
            for (j = i-1; j>=0 && arr[j] > key;j--){
                arr[j+1] = arr[j];
            }
            arr[j+1] = key;
        }
        Arrays.stream(arr).forEach(System.out::println);
    }

}
