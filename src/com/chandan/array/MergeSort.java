package com.chandan.array;

import java.util.Arrays;

/**
 * Step 1 − if it is only one element in the list it is already sorted, return.
 * Step 2 − divide the list recursively into two halves until it can no more be divided.
 * Step 3 − merge the smaller lists into new list in sorted order.
 *
 * Complexity
 * O(n log(n)) - time complexity
 * O(n) - space complexity
 */
public class MergeSort {
    public static void main(String[] args) {
        int[] a = new int[]{10, 9, 7, 101, 23, 44, 12, 78, 34, 23};
        mergeSort(a,0,9);
        Arrays.stream(a).forEach(System.out::println);
    }
    public static void mergeSort(int[] a, int beg, int end){
        int mid;
        if(beg<end)
        {
            mid = (beg+end)/2;
            mergeSort(a,beg,mid);
            mergeSort(a,mid+1,end);
            merge(a,beg,mid,end);
        }
    }
    public static void merge(int[] a, int beg, int mid, int end)
    {
        int i=beg,j=mid+1,k,index = beg;
        int[] temp = new int[10];
        while(i<=mid && j<=end)
        {
            if(a[i]<a[j])
            {
                temp[index] = a[i];
                i = i+1;
            }
            else
            {
                temp[index] = a[j];
                j = j+1;
            }
            index++;
        }
        if(i>mid)
        {
            while(j<=end)
            {
                temp[index] = a[j];
                index++;
                j++;
            }
        }
        else
        {
            while(i<=mid)
            {
                temp[index] = a[i];
                index++;
                i++;
            }
        }
        k = beg;
        while(k<index)
        {
            a[k]=temp[k];
            k++;
        }
    }
}
