package com.leetcode.easy;

/**
 * Example :-
 * Price    [7,1,5,3,6,4]
 * output max profit 5
 *
 *
 */
public class StockBuyAndSell {
    public static void main(String[] args) {
        // case 1;
        int []prices = {7,1,5,3,6,4};
        int profit = getMaxProfitWithN(prices);
        System.out.println("max profit is:-"+ profit);
        // case 2
        int []prices1 = {7,6,4,3,1};
        int profit1 = getMaxProfitWithN(prices1);
        System.out.println("max profit1 is:-"+ profit1);

    }
    public static int getMaxProfitWithN(int []prices) {
         int l = 0, r =1 ;
         int maxProfit = 0;
         while ( r < prices.length) {
             if ( prices[l] < prices[r] ) {
                 var profit = prices[r] - prices[l];
                 maxProfit = Math.max(maxProfit,profit);
             } else {
                 l = r;
             }
             r++;
         }
         return maxProfit;
    }
    // not efficient solution where time complexity O(n)2
    private static int getMaxProfit(int[] prices) {
        int sellIndex = 0, buyIndex = 0;
        int maxProfit = 0;
        for (int i = 0; i < prices.length; i++) {
            for (int j = i+1; j < prices.length; j++ ) {
                int diff = prices[j] - prices[i];
                if( diff > maxProfit ){
                    maxProfit = diff;
                    sellIndex = j;
                    buyIndex = i;
                }
            }
        }
        return maxProfit;
    }
}
