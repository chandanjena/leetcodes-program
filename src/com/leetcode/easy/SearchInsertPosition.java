package com.leetcode.easy;

/**
 * example
 * Input: nums = [1,3,5,6], target = 5
 * Output: 2
 *
 * Input: nums = [1,3,5,6], target = 2
 * Output: 1
 */
public class SearchInsertPosition {
    public static void main(String[] args) {
        int []arr = {1,3,5,6};
        int item = 5;
        System.out.println("position is :-"+ positionFounder(arr, item));
    }

    private static int positionFounder(int[] arr, int item) {
        int i = 0;
        while(arr[i] < item){
            i++;
            if(i == arr.length) {
                return arr.length;
            }
        }
        return i;
    }
}
