package com.leetcode.easy;

/**
 * Answer
 *     - Create two variables
 *         - i, our current location inside 's', initially 's.length() - 1'
 *         - length, the length of the last word
 *     - While 'i' is greater than or equal to 0 AND the current character
 *       at 'i' is equal to ' '
 *         - Decrement 'i'
 *     - While 'i' is greater than or equal to 0 AND the current characters
 *       at 'i' is not equal to ' '
 *         - Decrement 'i'
 *         - Increment 'length'
 *     - Return 'length'
 *
 * What is the Time and Space Complexity?
 *     - Time Complexity = O(n), where n is the length of the input string
 *         - O(n), visit each index once
 *     - Space Complexity = O(1)
 */
public class LastWordLength {
    public static void main(String[] args) {
        System.out.println(lastWordLength1("hello chandan"));
        System.out.println(lastWordLength2("hello chandan"));
    }

    private static int lastWordLength1(String s) {
        if(s.trim().isEmpty()){
            return 0;
        } else {
            String []array = s.split(" ");
            return array[array.length-1].length();
        }
    }
    public static int lastWordLength2(String s) {
        int i = s.length() - 1, length = 0;
        while (i >= 0 && s.charAt(i) == ' ') i--;
        while (i >= 0 && s.charAt(i) != ' ') {
            length++;
            i--;
        }
        return length;
    }

}
