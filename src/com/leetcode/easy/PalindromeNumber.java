package com.leetcode.easy;

/**
 * Palindrome of a number
 * 121 - true
 * -121 - false
 * 10 -false
 *
 * Algorithm
 *     - Lets validate the input parameter
 *         - if 'x' is negative,
 *             - Return false
 *     - Create two variables
 *         - copy, equals to 'x'
 *         - reverse, reverse form of 'x'
 *     - While 'copy' is greater than 0
 *         - Retrieve the rightmost digit
 *              - digit = copy % 10
 *         - Multiply 'reverse' by 10 and then increment by 'digit'
 *         - Remove rightmost digit
 *             - copy = copy / 10
 *     - Return true if 'reverse' is equal to 'x'
 *         - Else, return false
 *
 *   What is the Time and Space Complexity?
 *     - Time Complexity = O(logx), where x is the input value
 *         - O(logx), process each of the digits of 'x'
 *     - Space Complexity = O(1)
 */
public class PalindromeNumber {
    public static void main(String[] args) {
        //case 1 when 121 - return true
        System.out.println(isPalindrome(121));
        //case 2 when 120 - return false
        System.out.println(isPalindrome(120));
        //case 3 when -121 - return false
        System.out.println(isPalindrome(-121));
    }
    public static boolean isPalindrome(int number){
        if( number < 0) return false;
        int copy  = number, reverse = 0;
        while ( copy > 0){
            int rem = copy % 10;
            reverse = reverse * 10 + rem;
            copy = copy / 10;
        }
        return reverse == number;
    }
}
