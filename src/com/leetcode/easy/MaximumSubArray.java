package com.leetcode.easy;

/**
 * Answer
 *     - Create two variables
 *         - sum, the current running sum, initially 0
 *         - maxSum, the maximum subarray sum, initially 'Integer.MIN_VALUE'
 *     - Iterate through the elements of 'nums', denoted as 'num'
 *         - If 'sum' is less than 0
 *             - Reset 'sum' to 0 (we are starting a new subarray with 'num')
 *         - Increment 'sum' by 'num'
 *         - Update 'maxSum' if 'sum' is greater
 *     - Return 'maxSum'
 *
 * What is the Time and Space Complexity?
 *     - Time Complexity = O(n), where 'n' is the length of the input array
 *         - O(n), visit each element once
 *     - Space Complexity = O(1)
 */
public class MaximumSubArray {
    public static void main(String[] args) {
        System.out.println(maxSubArray(new int[]{-2,1,-3,4,-1,2,1,-5,4}));
    }

    private static int maxSubArray(int[] nums) {
        int sum = 0, maxSum = Integer.MIN_VALUE;

        for (int num: nums) {
            sum = Math.max(sum, 0) + num;
            maxSum = Math.max(maxSum, sum);
        }

        return maxSum;
    }
}
