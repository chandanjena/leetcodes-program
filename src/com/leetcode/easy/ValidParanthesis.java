package com.leetcode.easy;

import java.util.*;

/**
 * - Create a 'stack' to keep track of opening parentheses
 *     - Iterate through the indices of 's', denoted as 'i'
 *         - Retrieve the current character 'c' at index 'i'
 *         - If 'c' is an opening bracket
 *             - Push 'c' into the 'stack'
 *         - Else
 *             - If 'stack' is empty OR 'stack.pop()' (opening bracket) does
 *               not match 'c'
 *                 - Return false
 *     - Return true only if 'stack' is empty, which means all
 *       of the opening brackets are paired up with closing brackets
 *
 *    Complexity (tecnique 1)
 *    - Time Complexity = O(n), where n is the length of the input string
 *         - O(n), visit each index once
 *     - Space Complexity = O(n), where n is the length of the input string
 *          - O(n), stack
 */

public class ValidParanthesis {
    private static final String OPEN_BRACKETS = "([{";
    private static final String CLOSING_BRACKETS = ")]}";
    public static void main(String[] args) {
        String stringCase1 = "()";
        String stringCase2 = "()[]{}";
        String stringCase3 = "(]";
        String stringCase4 = "([)]";
        System.out.println(validateParanthesis2(stringCase1));
    }

    private static boolean validateParenthesis1(String s) {
        if(s.length() == 1){
            return false;
        }
        LinkedList<Character> stack = new LinkedList<>();
        Map<Character,Character> map = new HashMap<>();
        map.put('}','{');
        map.put(']','[');
        map.put(')','(');

        for(Character ch : s.toCharArray()){
            if(ch == ']' || ch == '}' || ch == ')') {
                if(stack.pollLast() !=  map.get(ch)) return false;
            } else stack.add(ch);
        }
        return stack.isEmpty();
    }

    public static boolean validateParanthesis2(String s) {
            Deque<Character> stack = new ArrayDeque<>();

            for (int i = 0; i < s.length(); i++) {
                char c = s.charAt(i);

                if (isOpeningBracket(c)) {
                    stack.push(c);
                } else {
                    if (stack.isEmpty() || !isMatching(stack.pop(), c)) {
                        return false;
                    }
                }
            }

            return stack.isEmpty();
        }

    private static boolean isOpeningBracket(char c) {
        return OPEN_BRACKETS.indexOf(c) != -1;
    }

    private static boolean isMatching(char open, char close) {
        return OPEN_BRACKETS.indexOf(open) == CLOSING_BRACKETS.indexOf(close);
    }

    }
