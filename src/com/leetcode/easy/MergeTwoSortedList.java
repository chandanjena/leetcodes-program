package com.leetcode.easy;

/**
 * merge two lists
 * // algorithm
 *  - Create the following variables
 *         - head, the sentinel head of the merged sorted list
 *         - tail, the sentinel tail of the merged sorted list
 *     - While 'l1' OR 'l2' is not equal to 'null'
 *         - If 'l2' is null OR 'l1' is not null AND 'l1.val' is less
 *           than 'l2.val'
 *             - Set 'tail.next' to 'l1'
 *             - Set 'tail' to 'l1'
 *             - Set 'l1' to 'l1.next'
 *         - Else
 *             - Set 'tail.next' to 'l2'
 *             - Set 'tail' to 'l2'
 *             - Set 'l2' to 'l2.next'
 *     - Return 'head.next'
 *  Complexity for mergeSortedList1()
 *  - Time Complexity = O(m + n), where m,n are the length of the
 *         two input list
 *         - O(m + n), visit each node once
 *     - Space Complexity = O(1)
 */
class ListNode {
    int val;
    ListNode next;
    ListNode() {}
    ListNode(int val) { this.val = val; }
    ListNode(int val, ListNode next) { this.val = val; this.next = next; }
  }
public class MergeTwoSortedList {
    public static void main(String[] args) {
        ListNode list1 = createList1();
        ListNode list2 = createList2();
       // ListNode finalList1 = mergeSortedList1(list1, list2);
        ListNode finalList2 = mergeSortedList2(list1, list2);
        while (finalList2 != null){
            System.out.println(finalList2.val);
            finalList2 = finalList2.next;
        }
    }

    private static ListNode mergeSortedList2(ListNode l1, ListNode l2) {
        if (l1 == null)
            return l2;
        if (l2 == null)
            return l1;

        ListNode root = new ListNode();
        // keep a copy pointer to root node to return.
        ListNode head = root;

        while (l1 != null  && l2 != null ){
            if (l1.val <= l2.val){
                root.next =l1;
                l1 = l1.next;
            } else {
                root.next =l2;
                l2 = l2.next;
            }
            root = root.next;
        }

        // add rest of of node when list of diffrent size
        if (l1 != null)
            root.next =l1;

        if (l2 != null)
            root.next =l2;

        return head.next;
    }

    private static ListNode mergeSortedList1(ListNode l1, ListNode l2) {
        ListNode head = new ListNode();
        ListNode tail = head;

        while (l1 != null || l2 != null) {
            if (l2 == null || (l1 != null && l1.val < l2.val)) {
                tail.next = l1;
                tail = l1;
                l1 = l1.next;
            } else {
                tail.next = l2;
                tail = l2;
                l2 = l2.next;
            }
        }

        return head.next;
    }

    private static ListNode createList1() {
        ListNode n1 = new ListNode(10);
        ListNode n2 = new ListNode(20);
        ListNode n3 = new ListNode(40);
        n1.next = n2;
        n2.next = n3;
        n3.next = null;
        return n1;
    }

    private static ListNode createList2() {
        ListNode n1 = new ListNode(11);
        ListNode n2 = new ListNode(33);
        ListNode n3 = new ListNode(44);
        n1.next = n2;
        n2.next = n3;
        n3.next = null;
        return n1;
    }
}
