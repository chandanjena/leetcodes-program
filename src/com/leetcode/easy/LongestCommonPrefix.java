package com.leetcode.easy;

import java.util.Arrays;

/**
 * Input: strs = ["flower","flow","flight"]
 * Output: "fl"
 *   algorithm
 *   ---------
 *    - Lets validate the input parameter first
 *         - if 'strs' is empty
 *             - return empty string
 *     - Create a variable 'length' to keep track of the current longest common
 *       prefix length, initially 'strs[0].length()'
 *     - Iterate through the indices from 1 -> strs.length - 1, denoted as 'i'
 *         - Update 'length' to 'strs[i].length()' if the current string is
 *           shorter
 *         - Iterate through the characters from 0 -> length - 1, denoted as 'j'
 *             - Retrieve the character 'c' and 'd' from 'strs[0]' and 'strs[i]'
 *             - If 'c' does not equal to 'd'
 *                 - Update 'length' to 'j'
 *                 - break out of loop
 *     - Return a substring of 'strs[0]' from '0 -> length - 1
 *     -----------------
 *     Complexity
 *      - Time Complexity = O(n * k) + O(k) = O(n * k), where n is the number of
 *         strings and k is the number of characters associated with each string
 *         - O(n * k), compare the first string with every other string
 *         - O(k), generate result substring
 *     - Space Complexity = O(k), where k is the number of characters
 *         associated with each string
 *         - O(k), resulting substring
 */
public class LongestCommonPrefix {
    public static void main(String[] args) {
      String [] string =  {"flower","flow","flight"};
      System.out.println("common string is :-" + longestCommonString1(string));
      System.out.println("common string is :-" + longestCommonString2(string));
    }

    private static String longestCommonString1(String[] string) {
        if(string.length < 0){
            return "";
        }
        int length = string[0].length();
        for(int i = 0; i < string.length; i++) {
            length = Math.min(length, string[i].length());

            for (int j = 0; j < length; j++) {
                char c = string[0].charAt(j), d = string[i].charAt(j);

                if (c != d) {
                    length = j;
                    break;
                }
            }
        }
        return string[0].substring(0, length);
    }

    /**
     * Second way of writing the code
     * @param string
     * @return
     */
    private static String longestCommonString2(String[] string) {
        // initially find the shortest length of first string in the array
        int shortestLength = string[0].length();
        //finding the smallest in the list
        for (String s : string){
            if(s.length() < shortestLength){
                shortestLength = s.length();
            }
        }
        // if no string are length more then 0means array contains all empty string
        // so return there is  no common match
        if (shortestLength == 0){
            return "";
        }
        //this variable is used to find the prefix common length
        int prefixLength = 0;
        // outer loop used to compare every character of smallest string with rest
        for(int i = 0; i <shortestLength; i++) {
            char c = string[0].charAt(i);
            // this loop is actually checking the each character with other
            // if not matched return the substring till prefix length
            for(String s :string){
                if(c != s.charAt(i)){
                    return string[0].substring(0, prefixLength);
                }
            }
            prefixLength++;
        }
        return string[0].substring(0, prefixLength);
    }
}
