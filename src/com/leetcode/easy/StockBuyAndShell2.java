package com.leetcode.easy;

/**
 *  Similar to its previous one but
 *  here we can do multiple transaction
 *  Example
 *  Input: prices = [7,1,5,3,6,4]
 * Output: 7
 * Explanation: Buy on day 2 (price = 1) and sell on day 3 (price = 5), profit = 5-1 = 4.
 * Then buy on day 4 (price = 3) and sell on day 5 (price = 6), profit = 6-3 = 3.
 */
public class StockBuyAndShell2 {
    public static void main(String[] args) {

        //case 1
        int []prices = {7,1,5,3,6,4};
        System.out.println(getProfit(prices));
        //case 2
        int []prices2 = {1,2,3,4,5,7};
        System.out.println(getProfit(prices2));
    }
    public static int getProfit(int []arr) {
        int profit = 0;
        for(int i = 1; i<arr.length; i++) {
            if (arr[i] > arr[i-1]) {
                profit += (arr[i] - arr[i-1]);
            }
        }
        return profit;
    }
}
