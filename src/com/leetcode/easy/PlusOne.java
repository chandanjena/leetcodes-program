package com.leetcode.easy;

import java.util.Arrays;

public class PlusOne {
    public static void main(String[] args) {
        int[] digits = {1,9,3};
        int[] result = onePlus(digits);
        Arrays.stream(result).forEach(System.out::print);
    }

    private static int[] onePlus(int[] digits) {
        int n = 1, last = digits.length;
        for(int i = last-1; i >= 0; i--) {
            if(digits[i] < 9) {
                digits[i] += 1;
                return digits;
            }
            digits[i] = 0;
        }
        int[] ans = new int[last +1];
        ans[0] = 1;
        return ans;
    }
}
