package com.leetcode.easy;

import java.util.Arrays;

/** Example
 *  [1,1,2] -> [1,2] - return 2
 * Answer
 *     - Create a variable 'i' to keep track of the current location
 *       we populating
 *     - Iterate through the indices of the input list, denoted as 'j'
 *         - Populate 'nums[j]' at 'nums[i]'
 *             - Increment 'i'
 *         - While 'j' is not at the last index AND 'nums[j + 1]' is equal
 *           to 'nums[j]'
 *             - Increment 'j'
 *     - Return 'i', the length of the new array without duplicates
 *
 * What is the Time and Space Complexity?
 *     - Time Complexity = O(n), where n is the length of the input array
 *         - O(n), visit each index once
 *     - Space Complexity = O(1)
 */
public class RemoveDuplicates {

  public static void main(String[] args) {
    System.out.println(removeDuplicates(new int[]{1,1,2}));
  }
  public static int removeDuplicates(int []arr){
    int i = 0;
    for (int j = 0; j < arr.length; j++){
      arr[i++] = arr[j];
      while (j != arr.length -1 && arr[j] == arr[j+1]) {
        j++;
      }
    }
    return i;
  }
}
