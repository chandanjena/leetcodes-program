package com.leetcode.easy;

import java.util.Arrays;
import java.util.HashMap;

/**
 * two sum mostly asked in every interview
 * input    nums = [2,7,11,15], sum = 9
 * ans      [0,1]
 *
 * algorithm
 * - Create a HashMap 'map' to keep track of element and index
 *     - Iterate through the indices of the input array 'nums', denoted as 'i'
 *         - If 'map' contains 'target - nums[i]'
 *             - Return { i, map.get(target - nums[i] }
 *         - Put 'nums[i]' and 'i' into 'map'
 *     - Return an empty array
 *
 *     complexity
 *      - Time Complexity = O(n), where n is the length of the input array
 *         - O(n), visit each index once
 *     - Space Complexity = O(n), where n is the length of the input array
 *         - O(n), map
 */
public class TwoSum {
    public static void main(String[] args) {
        int []arr = twoSum(new int[]{2,7,11,15}, 9);
        Arrays.stream(arr).forEach(System.out::println);
    }
    public static int[] twoSum(int [] nums, int target){
        //put(element, index)
        HashMap<Integer,Integer> map = new HashMap<>();
        for(int i = 0; i<nums.length; i++){
            if(map.containsKey(target-nums[i])){
                return new int [] {i, map.get(target-nums[i])};
            }
            map.put(nums[i], i);
        }
        return new int[0];
    }
}
